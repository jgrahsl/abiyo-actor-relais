#![allow(dead_code)]
#![allow(unused_imports)]

// This is a poll driver that provides a push interface while honouring hysteresis of an averaged value


#[macro_use]
extern crate log;
extern crate ascii;
extern crate pretty_env_logger;
extern crate structopt;
extern crate serial;

use std::io;
use std::io::prelude::*;
use std::str;
use std::{thread};
use std::time::{Duration, Instant};
use std::io::prelude::*;
use std::cell::RefCell;
use std::path::PathBuf;
use std::process::exit;

use uuid::Uuid;
mod cmdline;
use cmdline::Opt;
use structopt::StructOpt;

use ascii::AsciiChar;
use serial::prelude::*;
use rumqtt::{MqttClient, MqttOptions, QoS, Notification};
use serial::{SystemPort};

use crossbeam_channel::select;
use crossbeam_channel::{RecvTimeoutError};
use serde::{Deserialize, Serialize};

// MQTT basics
const MQTT_SERVER_PORT : u16 = 1883;

const MAX_NUM_RELAIS : usize = 16;

const CMD_TABLE  : [[[u8; 17]; 2] ; 16]= [
    [[58, 70, 69, 48, 53, 48, 48, 48, 48, 48, 48, 48, 48, 70, 68, 13, 10],
    [58, 70, 69, 48, 53, 48, 48, 48, 48, 70, 70, 48, 48, 70, 69, 13, 10]],
    [[58, 70, 69, 48, 53, 48, 48, 48, 49, 48, 48, 48, 48, 70, 67, 13, 10],
    [58, 70, 69, 48, 53, 48, 48, 48, 49, 70, 70, 48, 48, 70, 68, 13, 10]],
    [[58, 70, 69, 48, 53, 48, 48, 48, 50, 48, 48, 48, 48, 70, 66, 13, 10],
    [58, 70, 69, 48, 53, 48, 48, 48, 50, 70, 70, 48, 48, 70, 67, 13, 10]],
    [[58, 70, 69, 48, 53, 48, 48, 48, 51, 48, 48, 48, 48, 70, 65, 13, 10],
    [58, 70, 69, 48, 53, 48, 48, 48, 51, 70, 70, 48, 48, 70, 66, 13, 10]],
    [[58, 70, 69, 48, 53, 48, 48, 48, 52, 48, 48, 48, 48, 70, 57, 13, 10],
    [58, 70, 69, 48, 53, 48, 48, 48, 52, 70, 70, 48, 48, 70, 65, 13, 10]],
    [[58, 70, 69, 48, 53, 48, 48, 48, 53, 48, 48, 48, 48, 70, 56, 13, 10],
    [58, 70, 69, 48, 53, 48, 48, 48, 53, 70, 70, 48, 48, 70, 57, 13, 10]],
    [[58, 70, 69, 48, 53, 48, 48, 48, 54, 48, 48, 48, 48, 70, 55, 13, 10],
    [58, 70, 69, 48, 53, 48, 48, 48, 54, 70, 70, 48, 48, 70, 56, 13, 10]],
    [[58, 70, 69, 48, 53, 48, 48, 48, 55, 48, 48, 48, 48, 70, 54, 13, 10],
    [58, 70, 69, 48, 53, 48, 48, 48, 55, 70, 70, 48, 48, 70, 55, 13, 10]],
    [[58, 70, 69, 48, 53, 48, 48, 48, 56, 48, 48, 48, 48, 70, 53, 13, 10],
    [58, 70, 69, 48, 53, 48, 48, 48, 56, 70, 70, 48, 48, 70, 54, 13, 10]],
    [[58, 70, 69, 48, 53, 48, 48, 48, 57, 48, 48, 48, 48, 70, 52, 13, 10],
    [58, 70, 69, 48, 53, 48, 48, 48, 57, 70, 70, 48, 48, 70, 53, 13, 10]],
    [[58, 70, 69, 48, 53, 48, 48, 48, 65, 48, 48, 48, 48, 70, 51, 13, 10],
    [58, 70, 69, 48, 53, 48, 48, 48, 65, 70, 70, 48, 48, 70, 52, 13, 10]],
    [[58, 70, 69, 48, 53, 48, 48, 48, 66, 48, 48, 48, 48, 70, 50, 13, 10],
    [58, 70, 69, 48, 53, 48, 48, 48, 66, 70, 70, 48, 48, 70, 51, 13, 10]],
    [[58, 70, 69, 48, 53, 48, 48, 48, 67, 48, 48, 48, 48, 70, 49, 13, 10],
    [58, 70, 69, 48, 53, 48, 48, 48, 67, 70, 70, 48, 48, 70, 50, 13, 10]],
    [[58, 70, 69, 48, 53, 48, 48, 48, 68, 48, 48, 48, 48, 70, 48, 13, 10],
    [58, 70, 69, 48, 53, 48, 48, 48, 68, 70, 70, 48, 48, 70, 49, 13, 10]],
    [[58, 70, 69, 48, 53, 48, 48, 48, 69, 48, 48, 48, 48, 70, 70, 13, 10],
    [58, 70, 69, 48, 53, 48, 48, 48, 69, 70, 70, 48, 48, 70, 48, 13, 10]],
    [[58, 70, 69, 48, 53, 48, 48, 48, 70, 48, 48, 48, 48, 70, 69, 13, 10],
    [58, 70, 69, 48, 53, 48, 48, 48, 70, 70, 70, 48, 48, 70, 70, 13, 10]]];


fn dump(data : &Vec<u8>) {
    print!("-> dump: ");
    for i in data {

        match ascii::AsciiChar::from_ascii(*i) {
            Ok(ch) => print!("{}", ch),
            Err(_) => print!("_0x{:02x}", *i)
        };
    }

    println!("");
}

fn setup_serial_port(device : &PathBuf) -> SystemPort {
    let mut port = serial::open(device).unwrap();

    port.reconfigure(&|settings| {
        settings.set_baud_rate(serial::Baud9600)?;
        settings.set_char_size(serial::Bits8);
        settings.set_parity(serial::ParityNone);
        settings.set_stop_bits(serial::Stop1);
        settings.set_flow_control(serial::FlowNone);
        Ok(())
    }).unwrap();

    port.set_timeout(Duration::from_millis(1000)).unwrap();
    
    return port;
}

fn main() {
    pretty_env_logger::init();
    let opt = Opt::from_args();

    info!("Serial device{:?}", &opt.device);

    let randomized = format!("{}-{}", &opt.mqtt_client_id, Uuid::new_v4());
    println!("Registering as client {}", randomized);
    let mqtt_options = MqttOptions::new(&randomized, &opt.mqtt_broker, MQTT_SERVER_PORT).set_notification_channel_capacity(1000);
    let (mut mqtt_client, notifications) = MqttClient::start(mqtt_options).unwrap();

    let mut port = setup_serial_port(&opt.device); 

    for index in 0..MAX_NUM_RELAIS {
        let s = format!("{}/{}", &opt.mqtt_topic, index);
        println!("{}", s);
        mqtt_client.subscribe(&s, QoS::AtLeastOnce).unwrap();
    }

    println!("Starting event loop");

    let mut state : [Option<(usize, usize)>; MAX_NUM_RELAIS] = [None; MAX_NUM_RELAIS];

    loop {
        let notification = notifications.recv_timeout(Duration::from_millis(1000));
        match notification {
            Ok(Notification::Publish(publish)) => {
                println!("publish");

                let mut parts = publish.topic_name.split("/");

                // Bad: hardcoded parsing
                let _ns = parts.next();
                let _category = parts.next();
                let _type = parts.next();
                let index : usize= parts.next().unwrap().parse::<u8>().unwrap().into();

                let cmd : usize = match &(publish.payload)[..] {
                    b"0" => 0,
                    b"1" => 1,
                    _ => { println!("Unknown payload {}", str::from_utf8(&publish.payload).unwrap()); break}
                };

                print!("Relais {} to state {}, sending cmd: {}", index, cmd, str::from_utf8(&CMD_TABLE[index][cmd][..]).unwrap());
                port.write(&CMD_TABLE[index][cmd][..]).expect("Unable to write to serial");
                match state[index] {
                    None => println!("Updating state for {} with {}", index, cmd),
                    _ => (),
                }
                state[index] = Some((index, cmd));
            },
            Ok(Notification::PubAck(id)) => {println!("PubAck {:?}", id)},
            Ok(Notification::PubRec(id)) => {println!("PubRec {:?}", id)},
            Ok(Notification::PubRel(id)) => {println!("PubRel {:?}", id)},
            Ok(Notification::PubComp(id)) => {println!("PubComp {:?}", id)},
            Ok(Notification::SubAck(id)) => {println!("SubAck {:?}", id)},
            Ok(Notification::Disconnection) => {exit(0)},
            Err(RecvTimeoutError) => {
                state.iter().for_each( |o| {
                    if let Some(s) = o {
                        port.write(&CMD_TABLE[s.0][s.1][..]).expect("Unable to write to serial");
                    }
                })
            },
            _l => {
                println!("something else <{:?}>",_l); exit(0);}
        }
    }
}
