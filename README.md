# abiyo-actor-relais 

![License] ![ci-passing] ![rust-version]

## General

´abiyo-actor-relais` implements a trivial service for exposing control of a cheap USB relais board for driving wall plugs. The board accepts commands using USB packets via a simple request/response scheme. The service subscribes to a MQTT topic to which consumers of this service publish their relais setting requests. To date there is no known way to be for reading the state of the relais as set by the relais board.

## USB relais board protocol

`:FE050000FF00FE <13> <10>`
`:FE0500000000FD <13> <10>`


## Building overview

Yes, there is `cargo init` but this template adds

- integration with the gitlab CI
- docker-compose wrapper for building and running in a dockerized environment
- Trivial Makefile wrapper around docker-compose

## Building locally from source and unit testing

Building is done using the `cargo` command.

```bash
cargo build
```

```bash
cargo test
```

## Building using a containerized build environment

Instead of installing dependencies directly to your host, you can use the `rust-build:0.1.0-master` image from the [rust-build-agent] repository for building and testing.

The `Makefile` provides a convencience wrapper to the `docker-compose.yaml` which makes use of the dockerized build env. The repository root folder is mounted as a volume at `/src` inside the container.

To build using the dockerized toolchain:

```bash
make build
```

To drop into a shell containing some further dev-tools issue:

```bash
make shell
```

*Note*: To avoid fetching and building all dependencies on every start of the container, the `docker-compose.yaml` sets the `CARGO_HOME` env variable to locate
the cache for `cargo` in the folder `.cargo` inside the repository root folder.

## CI

The CI configuration for Gitlab `.gitlab-ci.yml` contains two stages: `build` and `test`.

`gitlab-ci.yml` is setup to require a runner tagged `gstrust`.

A compatible runner image is `rust-build:0.1.0-mster` from [rust-build-agent].

### Cache

To avoid downloading dependencies for each job, a cache can be configured in `gitlab-ci.yml` by defining globally:

```
cache:
  key: gst-rust-playground
  paths:
   - cargo
   - target

variables:
  CARGO_HOME: "${CI_PROJECT_DIR}/cargo"
  CARGO_TARGET_DIR: "${CI_PROJECT_DIR}/target"
```

This tells the gitlab-runner to take the `cargo` and `target` subfolders (relative to the `CI_PROJECT_DIR`) to move it (compressed) to the cache volume. On each job, the cache is picked from the volume, extracted into the `CI_PROJECT_DIR` and stored back after the job has finished.

The `variables:` are read by `cargo` set the locations explicitly since they are used by the `cache:` section.

*TODO*  do not handle `target` folder as cache but as an artifact.

### Speedup CI builds

For large amounts of dependencies and build artifacts, this procedure is very slow.

As a workaround, one can set the `cargo` and `target` folders to reside directly on the cache volume and exclude it from being handled as cache by gitlab-runner.

This is accomplished by instructing `cargo` to use custom locations:

```
cache:
  key: gst-rust-playground

variables:
  CARGO_HOME: "/cache/cargo"
  CARGO_TARGET_DIR: "/cache/target"
```

*Note*: learing the cache is not possible via gitlab or gitlab-runner. 

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.


[License]: https://img.shields.io/badge/license-MIT-brightgreen
[rust-version]: https://img.shields.io/badge/rust-1.43.0-yellowgreen
[ci-passing]: https://gitlab.com/jgrahsl/rust-template/badges/master/pipeline.svg
[rust-build-agent]: https://gitlab.com/jgrahsl/rust-build-agent
